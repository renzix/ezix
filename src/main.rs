//My attempt at a simple text editor in rust using GTK

extern crate gio;
extern crate gtk;
extern crate gdk;

//File io
use std::fs::File;
use std::io::{Read, Write};
use std::path::PathBuf;
//environment vars for app.run
use std::env::args;
//Mutex
use std::sync::{Arc, Mutex};
//use std::thread;
//use std::sync::mpsc::channel;
use std::ops::DerefMut;

use gio::prelude::*;
use gio::{ApplicationExt };
use gtk::{
    AboutDialog, AboutDialogExt, ContainerExt, FileChooserDialog, FileChooserAction, FileChooserExt, ResponseType, Window, DialogExt, GtkApplicationExt,
    GtkWindowExt, Inhibit, WidgetExt, TextView, TextBufferExt, TextViewExt
};

// make moving clones into closures more convenient
macro_rules! clone {
    (@param _) => ( _ );
    (@param $x:ident) => ( $x );
    ($($n:ident),+ => move || $body:expr) => (
        {
            $( let $n = $n.clone(); )+
            move || $body
        }
    );
    ($($n:ident),+ => move |$($p:tt),+| $body:expr) => (
        {
            $( let $n = $n.clone(); )+
            move |$(clone!(@param $p),)+| $body
        }
    );
}

//TODO Implement Copy Pasta
fn build_system_menu(application: &gtk::Application) {
    let menu_bar = gio::Menu::new();
    let file_menu = gio::Menu::new();
    let edit_menu = gio::Menu::new();
    let help_menu = gio::Menu::new();

    file_menu.append("New", "app.newfile");
    file_menu.append("Open", "app.openfile");
    file_menu.append("Save As", "app.saveasfile");
    file_menu.append("Close", "app.closefile");
    file_menu.append("Quit", "app.quit");
    menu_bar.append_submenu("File", &file_menu);

    edit_menu.append("Cut", "app.cut");
    edit_menu.append("Copy", "app.copy");
    edit_menu.append("Paste", "app.paste");
    edit_menu.append("Preferences", "app.pref");
    menu_bar.append_submenu("Edit", &edit_menu);

    help_menu.append("About", "app.about");
    menu_bar.append_submenu("Help", &help_menu);

    application.set_menubar(&menu_bar);
}



struct actions{
}

impl actions{
    fn new(application: &gtk::Application, textview: &gtk::TextView, window: &gtk::ApplicationWindow) -> actions{
        let action = actions{};
        action.add_actions(application, textview, window);
        action
    }

    //TODO Close Current Tab
    fn add_actions(self,application: &gtk::Application, textview: &gtk::TextView,
                   window: &gtk::ApplicationWindow) {
        let newfile = gio::SimpleAction::new("newfile", None);
        newfile.connect_activate(clone!(textview => move |_, _| {
            //self.new_file(&textview);
            println!("New File");
        }));

        let openfile = gio::SimpleAction::new("openfile", None);
        openfile.connect_activate(clone!(textview => move |_, _| {
            //self.open_file(&textview, None);
            println!("Open File")
        }));

        let saveasfile = gio::SimpleAction::new("saveasfile", None);
        saveasfile.connect_activate(clone!(textview => move |_, _| {
            //self.save_file(&textview, None);
            println!("Save file")
        }));

        /*let close = gio::SimpleAction::new("close", None);
        close.connect_activate(clone!(textview => move |_, _| {
        println!("Implement getting rid of textview"); 
    }));*/
        let quit = gio::SimpleAction::new("quit", None);
        quit.connect_activate(clone!(window => move |_, _| {
            window.destroy();
        }));

        //Keybindings!!!
        window.connect_key_press_event(clone!(window,textview => move |_,key| {
            //state is for ctrl/meta/mod while 
            let state = key.get_state();
            let key_pressed = gdk::keyval_to_unicode(key.get_keyval());
            //TODO Send State and Key_pressed and main window and focused widget to a library call which deals with keybindings.
            if state.contains(gdk::ModifierType::from_bits_truncate(4)) { //ctrl
                let key_pressed = key_pressed.unwrap();
                if key_pressed == 's'{
                    println!("savefile");
                    //self.save_file(&textview,None);
                }else if key_pressed == 'o'{
                    println!("openfile");
                    //self.open_file(&textview,None);
                }else if key_pressed == 'n'{
                    println!("newfile");
                    //self.new_file(&textview);
                }else if key_pressed == 'q'{
                    window.destroy();
                }else{
                    return Inhibit(false)
                }
                return Inhibit(true)
            }
            Inhibit(false)
        }));

        let about = gio::SimpleAction::new("about", None);
        about.connect_activate(clone!(window => move |_, _| {
            let p = AboutDialog::new();
            p.set_authors(&["Daniel DeBruno"]);
            p.set_website_label(Some("github.com/Renzix/ezix"));
            p.set_website(Some("http://github.com/Renzix/ezix"));
            p.set_title("About!");
            p.set_transient_for(Some(&window));
            p.run();
            p.destroy();
        }));

        application.add_action(&newfile);
        application.add_action(&openfile);
        application.add_action(&saveasfile);
        application.add_action(&about);
        application.add_action(&quit);
    }

    //TODO Error Handling
    fn save_file(&self, textview: &TextView, fname: Option<PathBuf>) -> bool{
        //Pick location and put it in fname
        let mut fname = fname.clone();
        if let None = fname {
            let dialog = FileChooserDialog::with_buttons::<Window>(
                Some("Save File"),
                None,
                FileChooserAction::Save,
                &[("_Cancel", ResponseType::Cancel), ("_Save", ResponseType::Accept)]
            );
            let res = dialog.run();
            dialog.emit_close();
            if res == ResponseType::Accept.into() {
                fname = dialog.get_filename();
                println!("Accepted File {:?}", fname);
            } else {
                println!("Cancelled!!!");
                return false;
            }
        }
        
        let fname = fname.expect("No file path selected");
        let buffer = textview.get_buffer().expect("Couldnt get buffer");
        let text_buffer = buffer.get_text(&buffer.get_start_iter(),&buffer.get_end_iter(),true).expect("Couldnt get text from buffer");
        //write textbuffer to a file
        let mut f = File::create(fname).expect("file cannot be written here");
        f.write_all(&text_buffer.into_bytes()).expect("File cannot be written to(has been created)");
        true
    }

    //TODO Error Handling
    fn new_file(&self, textview: &TextView) {
        textview.get_buffer().expect("Couldnt get textbuffer").set_text("");
    }
    
    //TODO Global Buffer for files and keep it open at all times (maybe)
    //TODO Error Handling (finish)
    fn open_file(&self,textview: &TextView, fname: Option<PathBuf>) -> bool{
        //Pick a location and put it in fname
        let mut fname = fname.clone();
        if let None = fname {
            let dialog = FileChooserDialog::with_buttons::<Window>(
                Some("Open File"),
                None,
                FileChooserAction::Open,
                &[("_Cancel", ResponseType::Cancel), ("_Open", ResponseType::Accept)]
            );
            let res = dialog.run();
            dialog.emit_close();
            if res == ResponseType::Accept.into() {
                fname = dialog.get_filename();
                println!("Accepted File {:?}", fname);
            } else {
                println!("Cancelled!!!");
                return false;
            }
        }
        let fname = fname.expect("filename");
        //Open file
        let mut f = match File::open(fname){
            Ok(f) => f,
            Err(e) => {
                println!("Error: {}\n with type: {:?}",e,e.kind());
                return true;
            }
        };
        //Get Data
        let mut data = String::new();
        match f.read_to_string(&mut data){
            Ok(f) => f,
            Err(e) => {
                println!("Error: {}\n with type: {:?}",e,e.kind());
                return false;
            }
        };
        let buffer = data;
        //Set buffer and return True
        textview.get_buffer().expect("Couldnt get textbuffer").set_text(&buffer);
        true
    }
}

impl Clone for actions{
    fn clone(&self) -> actions {
        actions {
        }
    }
}

impl Copy for actions{}

//TODO Implement Tabs
//TODO Blank UI with configurations for multiple types of widgets
fn build_ui(application: &gtk::Application) {
    let window = gtk::ApplicationWindow::new(application);

    window.set_title("Ezix");
    window.set_border_width(10);
    window.set_position(gtk::WindowPosition::Center);
    window.set_default_size(1440, 900);

    window.connect_delete_event(clone!(window => move |_, _| {
        window.destroy();
        Inhibit(false)
    }));
 
    let status_bar = gtk::Box::new(gtk::Orientation::Horizontal, 50);
    let texts = TextView::new();
    window.add(&texts);
    window.add(&status_bar);

    build_system_menu(application);
    let action = actions::new(application, &texts,&window);
    window.show_all();

}

fn main() {
    //File names is a vector of path bufs while file_lock is the mutex that manages that buffer
    let mut filenames: Vec<PathBuf> = Vec::new();
    let mut file_lock: Mutex<Vec<PathBuf>> = Mutex::new(filenames);
    //file_lock.try_lock().expect("Couldnt get lock").deref_mut().push(PathBuf::from("/home/genzix/Documents/reee"));
    println!("{:?}",file_lock);
    let application = gtk::Application::new("com.renzix.ezix",gio::ApplicationFlags::empty()).expect("Initialization failed...");
    application.connect_startup(move |app| {
        build_ui(app);
    });
    application.connect_activate(|_| {});
    application.run(&args().collect::<Vec<_>>());
}
