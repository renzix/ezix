# Hi
Made a simple text pad using GTK bindings in Rust. Im ready to attempt to abstract the whole thing. 

# Abstraction Idea

So main is where the program starts. Main is responsible to managing the UI. The UI is built from GTK Widgets which should be changable. There is a default layout which has specific GTK widgets and there is a way to move the widgets and save the layout. There also is a run bar which is toggable with keybindings. There is a config file which has your configuration in .config/ezix. There will (most likely) be a seperate library where you can define keybindings from which main reads from and is decided from the configuration. There will prob be 3 kinds normal, vi and emacs like.
